#include "grid_pedstrian_detecor.h"
#include "img_integral.h"

//#define  MAX(x, y) (x)>(y)?x:y;

GridPedestrianDetector::~GridPedestrianDetector()
{
	delete []classifier_array;
	delete []sum;
	delete []qsum;
	int i;
	for (i=0; i< vecShaarFeatSet.size(); i++)
	{
		delete []vecShaarFeatSet.at(i).dethaarfeatureset;
	}
}
 
 void GridPedestrianDetector::initial(int W, int H, const char* haarFilename,
	                                   const char* avergingimgname)
 {
	 float scale = 1.2F;	 
	 int count = 0;
	 int i,j ,curr_w , curr_h , ystep, xstep ,scaleidx;
	 
	 float curr_s = 1.0F;

	 imgwidth = W;
	 imgheight= H;
	 origPw = 16; origPh = 40;

	 minPw  = 32; minPh  = 80;
	 maxPw  = 40; maxPh  = 100;
	 region_lx = 0;
	 region_ty = 200;
	 region_rx = 650;
	 region_by = 400;
/*
	 minPw  = 16; minPh  = 40;
	 maxPw  = 40; maxPh  = 100;
	 region_lx = 6;
	 region_ty = 280;
	 region_rx = 740;
	 region_by = 450;

*/
    /* 
	 minPw  = 30; minPh  = 125;
	 maxPw  = 60; maxPh  = 150;
	 region_lx = 20;
	 region_ty = 20;
	 region_rx = 700;
	 region_by = 560;

	 // set for pets 2001 carm1
	 minPw  = 16; minPh  = 40;
	 maxPw  = 36; maxPh  = 90;
	 region_lx = 19;
	 region_ty = 265;
	 region_rx = 600;
	 region_by = 566;*/

	 loadhaarfeature(haarFilename);
	 calpmean(avergingimgname);
    
	 sum = new int[ (imgwidth+1)*(imgheight+1) ];
	 qsum = new Int64[ (imgwidth+1)*(imgheight+1)];
	 
	 for(; origPw*curr_s <= maxPw; curr_s*=scale)
	 {
		 if (origPw*curr_s < minPw)
			 continue;
		  curr_w = origPw * curr_s;
		  curr_w = ((curr_w + 1)>>1)<<1 ;
		  curr_h = (curr_w>>1) * 5;
		  ystep  = curr_h * 0.15;
		  xstep  = curr_w * 0.15;
             
		 ystep = MAX(ystep,2);
		 xstep = MAX(xstep,2);

		 ScaleHaarFeatureSet temp;
		 temp.objwidth  = curr_w;
		 temp.objheight = curr_h; 
         temp.dethaarfeatureset = new DetHaarFeature[WEAKCLASSIFIERNUM];
		 assert(temp.dethaarfeatureset);

		 float realscale = curr_w/(float)origPw;

         setall2DetFeature(origfeature, temp.dethaarfeatureset, realscale, sum);

		 vecShaarFeatSet.push_back(temp);
         
		 // create scale detector classifier
		 for(j = region_ty; j < region_by - curr_h; j+=ystep)
			 for(i = region_lx; i < region_rx - curr_w; i+=xstep)	  
				 count++;
	 
   }

	 printf("count %d\n now we need %f M Bytes",count,
		 sizeof(Online_Classifier)*count/1000000.0);

	 classifier_array = new Online_Classifier[count];
	 
	 if (!classifier_array)
	 {
		 printf("fail to malloc classifier_array");
		 exit(1);
	 }

	 curr_s = 1;
	 count = 0; 
	 scaleidx = 0;
   
	 for(; origPw * curr_s <= maxPw; curr_s *= scale)
	 {
		 if ( origPw*curr_s < minPw)
			 continue;
		 
		 curr_w = origPw * curr_s;
         // make the curr_w is even number
		 curr_w = ((curr_w + 1)>>1)<<1 ;
		 curr_h = (curr_w>>1) * 5;
		 ystep  = curr_h * 0.15;
		 xstep  = curr_w * 0.15;
		 
		 ystep = MAX(ystep,2);
		 xstep = MAX(xstep,2);
		 DetHaarFeature *temppt = vecShaarFeatSet.at(scaleidx++).dethaarfeatureset;
		 if(!temppt)
			 printf("temppt wrong\n");
         
	
		 // create scale detector classifier
		 for(j = region_ty; j < region_by - curr_h; j+=ystep)
			 for(i = region_lx; i < region_rx - curr_w; i+=xstep)	  
			 {
				
				 classifier_array[count].initial(i,j,curr_w,curr_h,temppt , pmeanvalue);			
				 count++;
			 }

	 }
	

	 patchcount = count;

 }

 void GridPedestrianDetector::calpmean(const char* averagimgname)
 {
    IplImage* origimg = cvLoadImage(averagimgname,CV_LOAD_IMAGE_GRAYSCALE);
	int lw = origimg->widthStep;
	int lh = origimg->height;
    int* sumo = new int[(lw + 1) * (lh + 1)];
	Int64* sqsumo = new Int64[(lw + 1) * (lh + 1)];
    int i , step;

	IMG_integral((unsigned char*)origimg->imageData , sumo , sqsumo , NULL , lw , lh);

	float area = (float)lw * (float)lh;
	float meanv , qssum , delata ,normfactor;
	
	meanv = sumo[(lw + 1) * (lh + 1) - 1];	
	meanv /= area;
	
	qssum = sqsumo[(lw + 1) * (lh + 1) -1];
	
	delata = sqrt(qssum/area - meanv*meanv) ;

	if (delata < 0.5F)
		delata = 0.5F;

    normfactor = 0.5F/delata;

    step = lw + 1;

	for (i=0; i < WEAKCLASSIFIERNUM; i++)
	{
		int *p0 ,*p1 ,*p2 ,*p3;
		float f;
		CH_SUM_OFFSETS(p0 , p1 , p2 ,p3 ,origfeature[i].rect[0], step, sumo);
		f = calc_sum4p(p0,p1,p2,p3,0) * origfeature[i].weight[0];

		CH_SUM_OFFSETS(p0 , p1 , p2 ,p3 ,origfeature[i].rect[1], step, sumo);
		f += calc_sum4p(p0,p1,p2,p3,0) * origfeature[i].weight[1];
		pmeanvalue[i] = f * normfactor;
	}


    delete []sumo;
	delete []sqsumo;
	cvReleaseImage(&origimg);
 }

 void GridPedestrianDetector::loadhaarfeature(const char* filename)
 {
	 FILE* fp = fopen(filename,"r");
     HaarFeature* origfpt = origfeature;
     float temp;
	 int count = 0;
	 while (fscanf(fp,"%f%f%f",&temp, &temp,&temp) != EOF)
	 {
		 int t;
         char name[16];
		 origfpt = origfeature + count++;
		 fscanf(fp,"%d%d%d%d%d%d",&origfpt->rect[0].x ,
			    &origfpt->rect[0].y,&origfpt->rect[0].width,&origfpt->rect[0].height,
				&t , &origfpt->weight[0]);
		 assert(t==0);

		 fscanf(fp,"%d%d%d%d%d%d%s",&origfpt->rect[1].x ,
			 &origfpt->rect[1].y,&origfpt->rect[1].width,&origfpt->rect[1].height,
				&t , &origfpt->weight[1] , name);
		 assert(t==0);
	 }
	 assert(count == WEAKCLASSIFIERNUM);
 }

 
 void GridPedestrianDetector::handle_Frame(unsigned char* imgdata, vector<CvRect>& Positionvec, 
	                                       float strongthre, ProssType type)
 {
	 IMG_integral(imgdata,sum,qsum,NULL,imgwidth,imgheight);

     // cal confidence
	 if(type == CAL_UPDATE)
	    cal_update_allclassifier(strongthre , Positionvec);
	 else if (type == UPDATEONLY)
		 updateallclassifier();
	 else
		 calConfidence(strongthre , Positionvec);

 }

 void GridPedestrianDetector::cal_update_allclassifier(float strongthre , vector<CvRect>& Postionvec)
 {
	 int count = patchcount ;
	 int step = imgwidth + 1;
	 Online_Classifier* classifier = classifier_array;
	 int objnum = 0;

	 for(int i = 0; i < count && objnum < 100; i++,classifier++)
	 {
		 int offset = 0;
		 float normfactor = classifier->cal_normfactor(step, sum , qsum ,offset);	 
		 float conf =classifier->cal_confidence(normfactor , offset);
			 
		 if (conf >= strongthre)
		 {
			 objnum ++;
			 CvRect tr;
			 tr.x = classifier->sx;
			 tr.y = classifier->sy;
			 tr.width = classifier->sw;
			 tr.height = classifier->sh;
			 Postionvec.push_back(tr);
			 classifier->updateOneSample(normfactor , offset ,flag);;
		}

		 
	 }
 }

 void GridPedestrianDetector::calConfidence(float strongthre,vector<CvRect>& Postionvec)
 {
	 int count = patchcount ;
	 int step = imgwidth + 1;
	 Online_Classifier* classifier = classifier_array;

	 for (int i = 0; i<count; i++,classifier++)
	 {	
		 int offset = 0;
		 float normfactor = classifier->cal_normfactor(step, sum , qsum ,offset);		 
		 float conf =classifier->cal_confidence(normfactor , offset);
		 if (conf >= strongthre)
		 {
			 CvRect tr;
			 tr.x = classifier->sx;
			 tr.y = classifier->sy;
			 tr.width = classifier->sw;
			 tr.height = classifier->sh;
			 Postionvec.push_back(tr);
		 }
	 }
 }

 void GridPedestrianDetector::updateallclassifier()
 {
	 int count = patchcount;
	 Online_Classifier* classifier = classifier_array;
     int step = imgwidth + 1;

	 for (int i=0; i<count; i++,classifier++)
	 {
		 int offset = 0;
		 /* mean while calculate the offset */
		 float normfactor = classifier->cal_normfactor(step, sum , qsum ,offset);
		 classifier->updateOneSample(normfactor , offset ,flag);
	 }

 }



 int  chRound( double val )
 {
	 double temp = val + 6755399441055744.0;
	 return (int)*((Int64*)&temp);
 }

 void GridPedestrianDetector::setall2DetFeature(HaarFeature* orig, DetHaarFeature* det,
	                                            float scale ,int* imgsum)
 {
    
	int i,k,step;
	float invsqscale = 1.0/(scale*scale);
    step = imgwidth + 1;

	for (i=0; i < WEAKCLASSIFIERNUM; i++)
	{
		HaarFeature* origt = orig + i;
		DetHaarFeature* dett = det + i;
		float sumw,area;

		for (k=0; k<HAAR_FEATURE_MAX; k++)
		{
			Ch_Rect tr;
			
			tr.x = chRound(origt->rect[k].x * scale);
			tr.y = chRound(origt->rect[k].y * scale);
			tr.width  = chRound(origt->rect[k].width * scale);
			tr.height = chRound(origt->rect[k].height * scale);

			CH_SUM_OFFSETS(dett->rect[k].p0,dett->rect[k].p1,dett->rect[k].p2,dett->rect[k].p3,
				           tr, step, imgsum);

			dett->weight[k] = origt->weight[k] * invsqscale;

			if (k==0)
				area = tr.width * tr.height;
			else
                sumw = dett->weight[k] * tr.width * tr.height;

		}
		det->weight[0] = -sumw / area;
      
	}
 }

 
  typedef  CvRect MyRect;
  typedef struct CvPTreeNode
  {
	  struct CvPTreeNode* parent;
	  MyRect* element;
	  int rank;
  }
  CvPTreeNode;
  typedef int (* ComFunc)(const void* a, const void* b);

  int is_equal( const void* _r1, const void* _r2)
  {
	  const MyRect* r1 = (const MyRect*)_r1;
	  const MyRect* r2 = (const MyRect*)_r2;
	  
	  int distance = chRound(r1->width*0.25);
	  
	  if( r2->x <= r1->x + distance &&
		  r2->x >= r1->x - distance &&
		  r2->y <= r1->y + distance &&
		  r2->y >= r1->y - distance &&
		  r2->width <= chRound( r1->width * 1.25 ) &&
		  chRound( r2->width * 1.25) >= r1->width )
	  {
		  return 1;
	  }
	  
	  return 0;
}

  int RectsPartition(vector<MyRect>* vecFaces, vector<int>* labels, ComFunc is_equal)
  {
	  assert(vecFaces && labels);
	  
	  int class_idx = 0;
	  vector<CvPTreeNode> *nodes = 0;
	  nodes = new vector<CvPTreeNode>;
	  nodes->clear();
	  
	  int i, j;
	  
	  // Initial O(N) pass. Make a forest of single-vertex trees.
	  for(i = 0; i < vecFaces->size(); i++)
	  {
		  CvPTreeNode node = {0, 0, 0};
		  node.element = &(vecFaces->at(i));
		  nodes->push_back(node);
	  }
	  
	  // The main O(N^2) pass. Merge connected components.
	  for(i = 0; i < nodes->size(); i++)
	  {
		  CvPTreeNode* node1 = &nodes->at(i);
		  CvPTreeNode* root = node1;
		  
		  while( root->parent )
			  root = root->parent;
		  
		  for(j = 0; j < nodes->size(); j++)
		  {
			  CvPTreeNode* node2 = &nodes->at(j);
			  
			  if( node2->element && node2 != node1 &&
				  is_equal( node1->element, node2->element))
			  {
				  CvPTreeNode* root2 = node2;
				  
				  // unite both trees
				  while( root2->parent )
					  root2 = root2->parent;
				  
				  if( root2 != root )
				  {
					  if( root->rank > root2->rank )
						  root2->parent = root;
					  else
					  {
						  root->parent = root2;
						  root2->rank += root->rank == root2->rank;
						  root = root2;
					  }
					  assert( root->parent == 0 );
					  
					  // compress path from node2 to the root
					  while( node2->parent )
					  {
						  CvPTreeNode* temp = node2;
						  node2 = node2->parent;
						  temp->parent = root;
					  }
					  
					  // compress path from node to the root
					  node2 = node1;
					  while( node2->parent )
					  {
						  CvPTreeNode* temp = node2;
						  node2 = node2->parent;
						  temp->parent = root;
					  }
				  }
			  }
		  }// end for j
	  }// end for i
	  
	  labels->clear();
	  for( i = 0; i < nodes->size(); i++ )
	  {
		  CvPTreeNode* node = &nodes->at(i);
		  int idx = -1;
		  
		  if( node->element )
		  {
			  while( node->parent )
				  node = node->parent;
			  if( node->rank >= 0 )
				  node->rank = ~class_idx++;
			  idx = ~node->rank;
		  }
		  labels->push_back(idx);
	  }
	  
	  nodes->clear();
	  delete nodes;
	  
	  return class_idx;
}

 void GridPedestrianDetector::postProcessobj(vector<CvRect>& Postionvec)
 {
	 int min_neighbors = 2;

	 vector<CvRect> tempFaces;
	 if( min_neighbors != 0 )
	 {
		 // group retrieved rectangles
		 vector<int> labels;
		 int nclasses = RectsPartition(&Postionvec, &labels, is_equal);
		 
		 int* neighbors = new int[nclasses];
		 MyRect *rects = new MyRect[nclasses];
		 memset(neighbors, 0, nclasses*sizeof(neighbors[0]));
		 memset(rects, 0, nclasses*sizeof(rects[0]));		
		 
		 // count number of neighbors
		 for(int i = 0; i < Postionvec.size(); i++)
		 {
			 MyRect r = Postionvec.at(i);
			 int idx = labels.at(i);
			 
			 neighbors[idx]++;
			 rects[idx].x += r.x;
			 rects[idx].y += r.y;
			 rects[idx].width += r.width;
			 rects[idx].height += r.height;
		 }

		 Postionvec.clear();
		 
		 // calculate average bounding box
		 //		vector<int> tempNeighbors;
		 //		tempNeighbors.clear();
		 for(int i = 0; i < nclasses; i++)
		 {
			 int n = neighbors[i];
			 if( n >= min_neighbors )
			 {
				 MyRect rect;
				 rect.x = (rects[i].x*2 + n) / (2*n);
				 rect.y = (rects[i].y*2 + n) / (2*n);
				 rect.width = (rects[i].width*2 + n) / (2*n);
				 rect.height = (rects[i].height*2 + n) / (2*n);
				 tempFaces.push_back(rect);
				 //				tempNeighbors.push_back(neighbors[i]);
			 }
		 }
		 delete[] neighbors;
		 delete[] rects;
		 
		 // filter out small face rectangles inside large face rectangles
		 for(int i = 0; i < tempFaces.size(); i++)
		 {
			 MyRect r1 = tempFaces.at(i);
			 //			int nb1 = tempNeighbors.at(i);
			 int flag = 1;
			 
			 //			printf("%d %d %d %d %d\n", r1.x, r1.y, r1.width, r1.height, nb1);
			 
			 for(int j = 0; j < tempFaces.size(); j++)
			 {
				 MyRect r2 = tempFaces.at(j);
				 //				int nb2 = tempNeighbors.at(j);
				 int distance = chRound(r2.width * 0.2);
				 
				 if( (i != j) &&
					 (r1.x >= r2.x) &&
					 (r1.y >= r2.y) && 
					 (r1.x + r1.width <= r2.x + r2.width) &&
					 (r1.y + r1.height <= r2.y + r2.height) )//&&
					 //					(nb2 > MAX(3, nb1) || nb1 < 3 ) )
				 {
					 flag = 0;
					 break;
				 }
			 }
			 
			 if( flag )
				 Postionvec.push_back(r1);
		 }
	}
 }

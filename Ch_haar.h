#ifndef CH_HAAR_H__
#define CH_HAAR_H__

typedef __int64 Int64;

typedef struct Ch_Rect
{
	int x, y;
	int width, height;
}Ch_Rect;

#define HAAR_FEATURE_MAX  		2

typedef struct HaarFeature
{
    Ch_Rect  rect[HAAR_FEATURE_MAX];
    int weight[HAAR_FEATURE_MAX];
	
}HaarFeature;

typedef struct DetHaarFeature
{
	struct
	{
		int *p0, *p1, *p2, *p3;
	}
	rect[HAAR_FEATURE_MAX];
	float weight[HAAR_FEATURE_MAX];
}
DetHaarFeature;

#define calc_sum4p(p0, p1, p2, p3, offset)	\
		((p0)[offset]+(p3)[offset]-((p1)[offset]+(p2)[offset]))
/*
* get sum image offsets for <rect> corner points 
* step - row step (measured in image pixels!) of sum image
*/
#define CH_SUM_OFFSETS( p0, p1, p2, p3, rect, step , img )                        \
    /* (x, y) */                                                                  \
    (p0) = (img) + (rect).x + (step) * (rect).y;                                  \
    /* (x + w, y) */                                                              \
    (p1) = (img) + (rect).x + (rect).width + (step) * (rect).y;                   \
    /* (x , y+h) */                                                               \
    (p2) = (img) + (rect).x + (step) * ((rect).y + (rect).height);                \
    /* (x + w, y + h) */                                                          \
    (p3) = (img) + (rect).x + (rect).width + (step) * ((rect).y + (rect).height);

void SetDetHaarFeature(HaarFeature* origfeat, DetHaarFeature* detfeat , int integralwidth);


#endif
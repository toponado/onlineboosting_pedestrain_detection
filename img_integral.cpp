#include <iostream>
using namespace std;
#include <assert.h>
#include "img_integral.h"


/*
*	integralImage()
*/

#define worktype	sumtype
#define cast_macro(x)	x
#define cast_sqr_macro(x)	((x)*(x))

void IMG_integral(srctype* srcImg, sumtype* sumOut, sqsumtype* sqsumOut,
				  sumtype* tiltedOut, int _imgwidth , int _imgheight)
{                                                             
	int x, y;                                   		           	
	sumtype s;                                            
	sqsumtype sq;                                             
	sumtype buf[MAX_IMAGE_WIDTH] = {0};  
	
	const srctype* src = srcImg;
	sumtype* sum = sumOut;
	sqsumtype* sqsum = sqsumOut;
	sumtype* tilted = tiltedOut;  
	int imgwidth   = _imgwidth;
	int imgheight  = _imgheight;  
	int srcstep    = imgwidth;
	int sumstep    = imgwidth + 1;
	int sqsumstep  = imgwidth + 1;
	int tiltedstep = imgwidth + 1;
    
	memset( sum, 0, (imgwidth+1)*sizeof(sum[0]));  
	sum += sumstep + 1;	
	if (sqsum)
	{
		memset(sqsum,0,(imgwidth+1)*sizeof(sqsum[0]));
		sqsum += sqsumstep + 1;
	}
	if (tilted)
	{
		memset(tilted,0,(imgwidth+1)*sizeof(tilted[0]));
		tilted += tiltedstep + 1;
	}

	if (sqsum == 0 && tilted == 0)
	{
	    for (y=0; y<imgheight; y++,src+=srcstep,sum+=sumstep)
	    {
			sum[-1] = 0;
			for (x=0,s=0; x<imgwidth; x++)
			{
				sumtype t = cast_macro(src[x]);
				s += t;
				sum[x] = sum[x - sumstep] + s;
			}
	    }
	}
	else if( tilted == 0)
	{
		for (y=0; y<imgheight; y++ , src += srcstep, sum+= sumstep,
			                         sqsum += sqsumstep)
		{
			sum[-1] = 0;
			sqsum[-1] = 0;
			for( x = 0, s = 0, sq = 0; x < imgwidth; x++ )
			{
				worktype it = src[x]; 
				sumtype t = cast_macro(it);                         
		        sqsumtype tq = cast_sqr_macro(it); 
				s += t;
				sq += tq;
				t = sum[x - sumstep] + s; 
				tq = sqsum[x - sqsumstep] + sq;
				sum[x] = t; 
				sqsum[x] = tq;
			}
		}
	}
	else
	{
		if( sqsum == 0 )  
		{
			assert(0);
			return;
		}
		sum[-1] = tilted[-1] = 0;
		sqsum[-1] = 0;

		for( x = 0, s = 0, sq = 0; x < imgwidth; x++ ) 
        {
			worktype it = src[x];
			sumtype t = cast_macro(it);
			sqsumtype tq = cast_sqr_macro(it);
			buf[x] = tilted[x] = t;
			s += t; 
			sq += tq;
			sum[x] = s;
			sqsum[x] = sq; 
		}

		if (imgwidth == 1)
            buf[1] = 0;

		for( y = 1; y < imgheight; y++ )                       
        {                                                        
            worktype it;                                         
            sumtype t0;                                          
            sqsumtype tq0;                                       
			
            src += srcstep;                                      
            sum += sumstep;                                      
            sqsum += sqsumstep;                                  
            tilted += tiltedstep;                                
			
            it = src[0/*x*/];                                    
            s = t0 = cast_macro(it);                             
            sq = tq0 = cast_sqr_macro(it);                       
			
            sum[-1] = 0;                                         
            sqsum[-1] = 0;                                       
            /*tilted[-1] = buf[0];*/                             
            tilted[-1] = tilted[-tiltedstep];                    
			
            sum[0] = sum[-sumstep] + t0;                         
            sqsum[0] = sqsum[-sqsumstep] + tq0;                  
            tilted[0] = tilted[-tiltedstep] + t0 + buf[1];       
			
            for( x = 1; x < imgwidth - 1; x++ )                
            {                                                    
                sumtype t1 = buf[x];                             
                buf[x-1] = t1 + t0;                              
                it = src[x];                                     
                t0 = cast_macro(it);                             
                tq0 = cast_sqr_macro(it);                        
                s += t0;                                         
                sq += tq0;                                       
                sum[x] = sum[x - sumstep] + s;                   
                sqsum[x] = sqsum[x - sqsumstep] + sq;            
                t1 += buf[x+1] + t0 + tilted[x - tiltedstep - 1]; 
                tilted[x] = t1;                                  
            }                                                    
			
            if( imgwidth > 1 )                                 
            {                                                    
                sumtype t1 = buf[x];                             
                buf[x-1] = t1 + t0;                              
                it = src[x];    /*+*/                            
                t0 = cast_macro(it);                             
                tq0 = cast_sqr_macro(it);                        
                s += t0;                                         
                sq += tq0;                                       
                sum[x] = sum[x - sumstep] + s;                   
                sqsum[x] = sqsum[x - sqsumstep] + sq;            
                tilted[x] = t0 + t1 + tilted[x - tiltedstep - 1]; 
                buf[x] = t0;                                     
            }                                                    
        }           
	}

}

/*
int main()
{
	unsigned char img[16];
	int sum[25];
	int qsum[25];
	int titlesum[25];
	int i,j;
    i = 1;
	for (j=0;j<16;j++)
		img[j] = i;

	IMG_integral(img,sum,qsum,titlesum,4,4);
	return 1;
}*/



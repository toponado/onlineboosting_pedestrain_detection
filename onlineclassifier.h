#ifndef ONLINECLASSIFIER_H__
#define ONLINECLASSIFIER_H__

#define WEAKCLASSIFIERNUM 400
#define SELECTORNUM 10
#define SELECTORSIZE 20

#include "haarweakclassifier.h"

class Online_Classifier
{
public:
	HaarWeakClassifier weakclassifier[WEAKCLASSIFIERNUM];
	short sx , sy , sw, sh;

	short selecotorset[SELECTORSIZE*SELECTORNUM];

	short selectedidx[SELECTORNUM];
    
    void  initial(int x, int y , int w , int h,DetHaarFeature* dethaarhead , 
		          float* pmeanarray);

	void  updateOneSample(const float normfactor , const int offset,int* flag );

	float cal_confidence(const float normfactor , const int offset);

	float cal_normfactor(int Integralwidth , int* sumimg , Int64* qsum ,
						int& offsetout);

};

   inline int possion(float lamda)
   {
	  return (int)(lamda+0.5);
   }


#endif
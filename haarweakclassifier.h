#ifndef HAARWEAKCLASSIFIER_H__
#define HAARWEAKCLASSIFIER_H__
#include <stdio.h>

#include "Ch_haar.h"
#include "Kalman_process.h"

class HaarWeakClassifier
{
public:
	Kalman neg;
	float  posmean;
	float  sumlamda[2]; //lamda_corr lamda_wrong  
	float  error;
	float  fvalue;
    DetHaarFeature *dfeat; // 负责计算特征值

    void initial(DetHaarFeature* dethaarfeature ,float _posmean);

	void caculate_featurevalue(int offset ,float normfactor);// the position info should be noted in dfeat
    
	// 根据 fvalue ,更新 分布， lamda_corr lamda_wrong ,error 
    void update(int k_lamda , float lamda);

	int decision();

	float hx_alpha(int offset ,float normfactor);

    inline	bool operator <(const HaarWeakClassifier& w)const
	{
		return error<w.error;
	}
	
	void PrintFeatureValue()
	{
		printf(" %d %f %f %f\n",this,error,sumlamda[0],sumlamda[1]);		
	}	

};
#endif

#ifndef _KALMAN_PROCESS_H__
#define _KALMAN_PROCESS_H__
#include <cassert>

class Kalman
{
public:
	float K;
	float R;
	float mean;
	float delta_sq;
	float P;
	
	Kalman(float _R=0.01F,float _P=1000,float _mean=0.0F,
		float _delta_sq=1.0F):R(_R),P(_P),mean(_mean),delta_sq(_delta_sq)
	{
		
	}
	
	inline void Process(float fx)
	{
		K=P/(P+R);
		mean=K*fx+(1-K)*mean;
		delta_sq=K*(fx-mean)*(fx-mean)+(1-K)*delta_sq;
		P=(1-K)*P;
		assert(delta_sq>=0);
	}
	
	
};
#endif


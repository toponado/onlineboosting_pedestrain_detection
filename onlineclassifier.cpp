#include <list>
#include <ctime>
#include <cmath>
using namespace std;

#include "onlineclassifier.h"

void Online_Classifier::initial(int x, int y , int w , int h, 
								DetHaarFeature* dethaarhead ,float *pmarray )
{

	int i;
	srand(time(NULL));


	for (i = 0; i < WEAKCLASSIFIERNUM ; i++)
		weakclassifier[i].initial(dethaarhead + i,pmarray[i]);


	int count = WEAKCLASSIFIERNUM;
	for (i = 0; i < SELECTORNUM*SELECTORSIZE ; i++)
		selecotorset[i] = rand()%count;


    count /= 2;
    for (i = 0; i < SELECTORNUM; i++)
		selectedidx[i] = rand()%count; 

	sx = x; sy = y;
	sw = w; sh = h;


}

float Online_Classifier::cal_normfactor(int Integralwidth , int* sumimg , Int64* qsum ,
										int& offsetout)
{
	int offset , tempsum;
	float area = (float)sw*(float)sh;
	float meanv , qssum , delata ,normfactor;
	
	offset = sy * Integralwidth + sx ;
	tempsum = sh * Integralwidth;
	
	meanv = sumimg[offset + tempsum + sw] + sumimg[offset]
		- sumimg[offset + tempsum] - sumimg[offset + sw];
	
	meanv /= area;
	
	qssum = qsum[offset + tempsum + sw] + qsum[offset]
		- qsum[offset + tempsum] - qsum[offset + sw];
	
	delata = sqrt(qssum/area - meanv*meanv);
	if (delata < 0.5F)
		delata = 0.5F;

    normfactor = 0.5F/delata;
    offsetout = offset;
	return normfactor;

}

float Online_Classifier::cal_confidence(const float normfactor , const int offset)
{
  
    
   float area = (float)sw*(float)sh;
   int i ;
   float confsum = 0.0F; 
   
   for (i=0; i<SELECTORNUM; i++)
   {
	   confsum += weakclassifier[selectedidx[i]].hx_alpha(offset , normfactor);
   }
   return confsum;
}

void Online_Classifier::updateOneSample(const float normfactor , const int offset,
										int* flag )
{
	int n,random,k_lamda,count;
    int Nselector = SELECTORNUM;

	count = WEAKCLASSIFIERNUM;

	memset(flag,0, sizeof(int)*count);
	list<int> idxlst;

	for (n=0; n<count ; n++)
		idxlst.push_back(n);

    for (n=0; n< SELECTORSIZE ; n++)
		idxlst.remove(selecotorset[n]);

	float lamda = 1.0F;  

    HaarWeakClassifier* classifierhead = weakclassifier;
	for (n=0;n<Nselector;n++)
	{
		k_lamda=possion(lamda);
		int endidx = (n+1)*SELECTORSIZE;
		for (int i= n*SELECTORSIZE ; i < endidx ; i++)
		{
			int t = selecotorset[i];
            HaarWeakClassifier* classifier = classifierhead + t;
			if (!flag[t])
			{
				classifier->caculate_featurevalue(offset,normfactor);
				flag[t] = 1;
			}		
			classifier->update(k_lamda,lamda);		
		}

		int idxmin , idxmax ,ip;
		float mine, maxe;
		idxmin = idxmax = endidx - SELECTORSIZE;
		mine = maxe = classifierhead[selecotorset[idxmin]].error;
	    
		for (ip = idxmin + 1; ip < endidx; ip++)
		{
			int t = selecotorset[ip];
			float local_error = classifierhead[t].error;

			if (local_error < mine)
			{
				mine = local_error;
				idxmin = ip;
			}
			else if(local_error > maxe)
			{
				maxe = local_error;
				idxmax = ip;
			}
		}
		if (idxmax == idxmin)
		{
			idxmax = idxmin + 1;
			if (idxmax>=endidx)
			{
				idxmax = idxmin - 1;
			}
		}

		if (mine>0.5F||mine<0.000000001F)
			return ;

		int t = selecotorset[idxmin];		
		    selectedidx[n] = t;

		if (!classifierhead[t].decision())
			lamda /=( 2.0F * (1 - mine) );
		else 
			lamda /=( 2.0F * mine);

		//classifierhead[selecotorset[idxmin]]->alpha

	     t = selecotorset[idxmax];
		 random = *(idxlst.begin());
		 
		 idxlst.remove(random);
		 idxlst.push_back(t);
		 
		 selecotorset[idxmax] = random;		 
         classifierhead[random].sumlamda[0] = classifierhead[random].sumlamda[1] = 1.0F;
 
	}
   
   
}
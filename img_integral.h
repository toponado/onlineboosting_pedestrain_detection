#ifndef _IMG_INTEGRAL
#define _IMG_INTEGRAL


#define MAX_IMAGE_WIDTH	3000

#define srctype		unsigned char
#define sumtype		int

#define sqsumtype	__int64

//#define sqsumtype	int
/**
* integral image
*
* @param srcImg:	the source 8-bit gray image
* @param sum:	the integral sum of image
* @param sqsum:	the integral sum of squared image
* @param tilted:	the integral sum of tilted image
* @param szImg:	the size of image
* @return: none
**/

void IMG_integral( srctype* srcImg, sumtype* sumOut, sqsumtype* sqsumOut,
				  sumtype* tiltedOut, int imgwidth , int imgheight);

#endif //_IMG_INTEGRAL


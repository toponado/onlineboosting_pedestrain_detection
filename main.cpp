#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

using namespace cv;
using namespace std;

#include <ctime>

#include "grid_pedstrian_detecor.h"
#include "config_imgsersies.h"


int main()
{
	int idx=Begin_idx;
	IplImage *srcImg , *grayimg;
	char full_imgname[256],name[50];
	int imgwdith , imgheight;
	vector<CvRect> rectvec;
	vector<CvRect>::iterator iterp , iterend;
	clock_t begin , end;
	double duration;
	
	rectvec.reserve(1000);

	CREATEIMGNAME
	printf("%s\n",full_imgname);
    srcImg = cvLoadImage(full_imgname);

	if (!srcImg)
	{
		printf("fail to open %s " , full_imgname);
		return 1;
	}

	imgwdith  = srcImg->width;
	imgheight = srcImg->height;

	grayimg = cvCreateImage(cvSize(imgwdith,imgheight), 8 , 1);

	cvNamedWindow( "show", 0 );
	cvShowImage( "show", srcImg );
	cvWaitKey(1);	

	cvReleaseImage(&srcImg);

	GridPedestrianDetector *gridDetetor = new GridPedestrianDetector;

	gridDetetor->initial(imgwdith , imgheight , "train_result.txt","average.bmp");

  
	for (idx=Begin_idx+SampleRate;idx<=End_idx;idx+=SampleRate)
	{
        begin = clock();
		rectvec.clear();
		
		CREATEIMGNAME
	//	printf("%s",full_imgname);
	    srcImg=cvLoadImage(full_imgname);

		if (!srcImg) continue;			
		
		cvCvtColor(srcImg,grayimg,CV_BGR2GRAY);

		ProssType t = idx - Begin_idx <= 4 ? UPDATEONLY : CAL_UPDATE;
		
		gridDetetor->handle_Frame((unsigned char*)grayimg->imageData,rectvec,0,t);

		gridDetetor->postProcessobj(rectvec);

		iterend = rectvec.end();
		for (iterp = rectvec.begin(); iterp != iterend ; iterp++)
		{
			cvRectangle(srcImg,cvPoint(iterp->x,iterp->y) , cvPoint(iterp->x + iterp->width,iterp->y+iterp->height),
				cvScalar(0,255,255),2);
		}

		cvRectangle(srcImg,cvPoint(gridDetetor->region_lx,gridDetetor->region_ty),
			               cvPoint(gridDetetor->region_rx,gridDetetor->region_by),
						   cvScalar(0,0,255),2);/**/

		end = clock();
		duration = (end - begin)/(double)CLOCKS_PER_SEC;
		printf("%s use %lf seconds \n",name,duration);

		cvShowImage("show",srcImg);
		int c =cvWaitKey(10);
		if (c==27)
		{
			cvReleaseImage(&srcImg);
			break;
		}

		
		CREATESAVENAME
	//	cvSaveImage(full_imgname,srcImg);
		cvReleaseImage(&srcImg);
	}
	
	
	cvDestroyWindow("show");
	cvReleaseImage(&grayimg);
	delete gridDetetor;
	return 1;







		 

}



#include "Ch_haar.h"


void SetDetHaarFeature(HaarFeature* origfeat, DetHaarFeature* detfeat,
					   int integralwidth,int* sum)
{
	CH_SUM_OFFSETS(detfeat->rect[0].p0,detfeat->rect[0].p1,detfeat->rect[0].p2,detfeat->rect[0].p3,
		          origfeat->rect[0],integralwidth,sum);

	CH_SUM_OFFSETS(detfeat->rect[1].p0,detfeat->rect[1].p1,detfeat->rect[1].p2,detfeat->rect[1].p3,
		          origfeat->rect[1],integralwidth,sum);

}


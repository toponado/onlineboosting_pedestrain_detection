#include <cmath>

#include "haarweakclassifier.h"

void HaarWeakClassifier::initial(DetHaarFeature* dethaarfeature ,float _posmean)
{
	dfeat = dethaarfeature;
	posmean = _posmean;
	error = 0.5;
	sumlamda[0] = sumlamda[1] = 1;
}

void HaarWeakClassifier::caculate_featurevalue(int offset ,float normfactor)
{
   fvalue = calc_sum4p(dfeat->rect[0].p0,dfeat->rect[0].p1,dfeat->rect[0].p2,
	         dfeat->rect[0].p3,offset) * dfeat->weight[0];

   fvalue += calc_sum4p(dfeat->rect[1].p0,dfeat->rect[1].p1,dfeat->rect[1].p2,
	         dfeat->rect[1].p3,offset) * dfeat->weight[1];

   fvalue *= normfactor;
   return;

}
void HaarWeakClassifier::update(int k_lamda , float lamda)
{
	int count = k_lamda;
	for(int i=0; i<count; i++)
		neg.Process(fvalue);
    int y = decision();

	sumlamda[y] += lamda;
	error = sumlamda[1]/(sumlamda[0] + sumlamda[1]);

	return;

}

float HaarWeakClassifier::hx_alpha(int offset ,float normfactor)
{
	caculate_featurevalue(offset , normfactor);

	float y = 2*(float)decision() -1 ;
	
	return y* 0.5F* (float)log( (1-error)/error );
}

int HaarWeakClassifier::decision()
{
	float thre = (posmean + neg.mean)/2.0F;
	float y = (posmean - neg.mean) * (fvalue - thre) ;
    
	return y>0 ? 1 : 0; 
}
/*
/*
[   (x-mean1)^2     (x-mean2)^2  ]     ( delta_sq2)
exp[- ------------ +  ------------- ]*sqrt(----------) >=1.0F
[   2*delta_sq1     2*delta_sq2  ]     ( delta_sq1)
*/
/*
int HaarWeakClassifier::decision()

{
   
	float result;
	float posdelata = neg.delta_sq / 4.0F;
	
		result=exp(0.5*(-(fvalue-posmean)*(fvalue-posmean)/posdelata +
			(fvalue-neg.mean)*(fvalue-neg.mean)/neg.delta_sq))
			*sqrt(neg.delta_sq/posdelata);
		
		return result>1?1:0;  

}*/
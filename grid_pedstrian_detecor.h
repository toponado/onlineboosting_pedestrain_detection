#ifndef GRID_DETECTOR_H
#define GRID_DETECTOR_H

#include "Ch_haar.h"
#include "onlineclassifier.h"

#include <vector>
using namespace std;

#include <cv.h>
#include <cxcore.h>
#include <highgui.h>


 enum ProssType { CALONLY, UPDATEONLY, CAL_UPDATE};

typedef struct ScaleHaarFeatureSet 
{
	DetHaarFeature* dethaarfeatureset; // length = WEAKCLASSIFIERNUM
	int objwidth , objheight;

}ScaleHaarFeatureSet;

class GridPedestrianDetector
{
public:

	~GridPedestrianDetector();
	int imgwidth , imgheight;
	int origPw, origPh;
	int minPw, minPh;
	int maxPw, maxPh;
	int region_lx , region_ty , region_rx , region_by;
	
	float pmeanvalue[WEAKCLASSIFIERNUM];
	HaarFeature origfeature[WEAKCLASSIFIERNUM];
    vector<ScaleHaarFeatureSet> vecShaarFeatSet;

	int flag[WEAKCLASSIFIERNUM];

	Online_Classifier *classifier_array;  // it is the principal memory cost
    int patchcount ;

	int   *sum;
    Int64 *qsum;

	void initial(int W, int H, const char* haarFilename,const char* avergingimgname );
    
    void handle_Frame(unsigned char* imgdata, vector<CvRect>& Positionvec,
		              float strongthre , ProssType type);

	void calConfidence(float strongthre,vector<CvRect>& Postionvec);

	void updateallclassifier();

	void cal_update_allclassifier(float strongthre , vector<CvRect>& Postionvec);

	void setall2DetFeature(HaarFeature* orig, DetHaarFeature* det,float scale ,int* imgsum);

	void loadhaarfeature(const char* filename);

    // we calculate the mean value of each haar feature from an averaging position image
	void calpmean(const char* averagimgname);

	void postProcessobj(vector<CvRect>& Postionvec);

};
#endif